var lista = [];
var numAciertos = 0;

$(document).ready(function () {
    $('#btnReiniciar').addClass('disabled')
    loadPet();
    $("#img1").draggable();
    $("#img2").draggable();
    $("#img3").draggable();
    $("#res1").droppable({
        drop: function (event, ui) {
            if (lista[0] == ui.draggable.attr('name')) {
                $(this).css('background', 'rgb(0,200,0)');
                $('#' + ui.draggable.attr('id')).draggable({revert: "invalid"});
                numAciertos++;
                validar(numAciertos);
            } else {
                $('#' + ui.draggable.attr('id')).draggable({revert: "valid"});
            }

        }
    });
    $("#res2").droppable({
        drop: function (event, ui) {
            if (lista[1] == ui.draggable.attr('name')) {
                $(this).css('background', 'rgb(0,200,0)');
                $('#' + ui.draggable.attr('id')).draggable({revert: "invalid"});
                numAciertos++;
                validar(numAciertos);
            } else {
                $('#' + ui.draggable.attr('id')).draggable({revert: "valid"});
            }
        }
    });

    $("#res3").droppable({
        drop: function (event, ui) {
            if (lista[2] == ui.draggable.attr('name')) {
                $(this).css('background', 'rgb(0,200,0)');
                $('#' + ui.draggable.attr('id')).draggable({revert: "invalid"});
                numAciertos++;
                validar(numAciertos);
            } else {
                $('#' + ui.draggable.attr('id')).draggable({revert: "valid"});
            }
        }
    });
});

function loadPet() {
    var breedJSON;
    $.ajax({
        url: "assets/js/data.json",
        type: 'GET',
        beforeSend: function () {

        },
        success: function (data) {
            breedJSON = data;
            loadBreed(breedJSON['breed'][7].name, "mainImg");
            loadBreed(breedJSON['breed'][5].name, "img1");
            $('#img1').attr('name', breedJSON['breed'][5].name);
            loadBreed(breedJSON['breed'][6].name, "img2");
            $('#img2').attr('name', breedJSON['breed'][6].name);
            loadBreed(breedJSON['breed'][2].name, "img3");
            $('#img3').attr('name', breedJSON['breed'][2].name);
            lista.push(breedJSON['breed'][5].name);
            lista.push(breedJSON['breed'][6].name);
            lista.push(breedJSON['breed'][2].name);

            lista = lista.sort(function () {
                return Math.random() - 0.5
            });

            $('#res1').text(lista[0]);
            $('#res2').text(lista[1]);
            $('#res3').text(lista[2]);
            loadModal(lista[0], breedJSON,'modalImg1');
            loadModal(lista[1], breedJSON,'modalImg2');
            loadModal(lista[2], breedJSON,'modalImg3');
        },
        error: function () {
            alert('Error peticion Ajax');
        }
    }).done(function (e) {

    });
}
function loadBreed(breed, id) {
    $.ajax({
        url: "https://dog.ceo/api/breed/" + breed + "/images/random",
        type: 'GET',
        beforeSend: function () {

        },
        success: function (data) {
            $("#" + id).attr("src", data.message);
        },
        error: function (e) {
            alert('Error peticion Ajax' + e);
        }
    }).done(function (e) {

    });
}

function validar(num) {
    if (num == 3) {
        $('#btnReiniciar').removeClass('disabled');
        $('#btnModalImg1').removeAttr('disabled');
        $('#btnModalImg2').removeAttr('disabled');
        $('#btnModalImg3').removeAttr('disabled');
    }
}

function loadModal(breed, json,id) {
    for (i in json.breed) {
        if(json.breed[i].name == breed){
            msg = '<div class="modal-dialog">' +
                    '<div class="modal-content">' +
                        '<div class="modal-header">' +
                        '<h4 class="modal-title">'+breed+'</h4>' +
                        '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
                        '</div>' +
                        '<div class="modal-body">' +
                        '<p>'+json.breed[i].description+'</p>' +
                        '</div>' +
                        '<div class="modal-footer">' +
                        '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                        '</div>' +
                    '</div>' +
                '</div>';
            $('#' + id).html(msg);
        }


    }

}
